%global   forgeurl https://github.com/nwg-piotr/nwg-shell
%global   sys_name nwg_shell


Name:           nwg-shell
Version:        0.5.46
%forgemeta
Summary:        Meta-package, installer and updater
Release:        1%{?dist}

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       python-gobject
Requires:       python-dasbus
Requires:       gtk3
Requires:       foot
Requires:       gnome-themes-extra
Requires:       grim
Requires:       ImageMagick
Requires:       jq
Requires:       libappindicator-gtk3
Requires:       light
Requires:       font(dejavusans)
Requires:       font(dejavusansmono)
Requires:       font(dejavuserif)
Requires:       network-manager-applet
Requires:       papirus-icon-theme
Requires:       playerctl
Requires:       pulseaudio-utils
Requires:       PolicyKit-authentication-agent
Requires:       python-geopy
Requires:       python-yaml
Requires:       slurp
Requires:       swappy
Requires:       sway
Requires:       swayidle
Requires:       swaylock
Requires:       swaybg
Requires:       wl-clipboard
Requires:       xorg-x11-server-Xwayland
Requires:       wlsunset
Requires:       azote
Requires:       gopsuinfo
Requires:       nwg-bar
Requires:       nwg-dock
Requires:       nwg-drawer
Requires:       nwg-menu
Requires:       nwg-look
Requires:       nwg-panel
Requires:       nwg-shell-config
Requires:       nwg-shell-wallpapers
Requires:       nwg-displays
Requires:       swaync
Requires:       gtklock
Requires:       gtklock-userinfo-module
Requires:       gtklock-powerbar-module
Requires:       gtklock-playerctl-module

Recommends:     thunar

Suggests:       chromium
Suggests:       mousepad

%description
%{summary}

%prep
%forgeautosetup -p1

%build
%py3_build
for lib in scripts/*; do
sed --in-place "1 s:(#!)s*/usr.*:1%{__python3}:" $lib
done

%install
%py3_install
for lib in %{buildroot}%{python3_sitelib}/%{sys_name}/*.py; do
sed --in-place '1{/^#!.*python/d}' $lib
done
install -D -t %{buildroot}%{_bindir} scripts/screenshot
install -D -t %{buildroot}%{_datadir}/backgrounds nwg-shell.jpg


%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{python3_sitelib}/%{sys_name}/
%{python3_sitelib}/%{sys_name}-%{version}-py%{python3_version}.egg-info/
%{_datadir}/backgrounds/*

%changelog
* Thu Feb 20 2025 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.46-1
- Update to 0.5.46

* Mon Feb 17 2025 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.45-1
- Update to 0.5.45

* Fri Dec 20 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.44-1
- Update to 0.5.44

* Tue Dec 10 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.43-1
- Update to 0.5.43

* Tue Nov 19 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.42-1
- Update to 0.5.42

* Mon Nov 18 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.41-1
- Update to 0.5.41

* Sun Nov 17 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.40-1
- Update to 0.5.40

* Fri Nov 01 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.39-1
- Update to 0.5.39

* Tue Aug 20 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.38-1
- Update to 0.5.38

* Wed Aug 07 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.37-1
- Update to 0.5.37

* Wed Jul 03 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.36-1
- Update to 0.5.36

* Wed Jun 12 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.35-1
- Update to 0.5.35

* Fri Apr 19 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.34-1
- Update to 0.5.34

* Fri Mar 01 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.33-1
- Update to 0.5.33

* Mon Feb 05 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.32-1
- Update to 0.5.32

* Tue Jan 30 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.31-1
- Update to 0.5.31

* Mon Jan 29 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.30-1
- Update to 0.5.30

* Sat Jan 27 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.29-1
- Update to 0.5.29

* Thu Jan 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.28-1
- Update to 0.5.28

* Tue Jan 09 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.27-1
- Update to 0.5.27

* Thu Jan 04 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.25-1
- Update to 0.5.25

* Sat Dec 30 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.24-1
- Update to 0.5.24

* Sat Dec 02 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.23-1
- Update to 0.5.23

* Fri Nov 17 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.22-1
- Update to 0.5.22

* Wed Nov 15 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.20-1
- Update to 0.5.20

* Fri Nov 03 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.19-1
- Update to 0.5.19

* Thu Nov 02 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.18-1
- Update to 0.5.18

* Thu Oct 19 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.17-1
- Update to 0.5.17

* Wed Oct 18 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.16-1
- Update to 0.5.16

* Wed Oct 11 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.14-1
- Update to 0.5.14

* Tue Oct 10 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.13-1
- Update to 0.5.13

* Thu Oct 05 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.12-1
- Update to 0.5.12

* Tue Oct 03 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.11-1
- Update to 0.5.11

* Fri Sep 15 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.10-1
- Update to 0.5.10

* Fri Sep 01 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.9-1
- Update to 0.5.9

* Thu Aug 03 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.7-1
- Update to 0.5.7

* Tue Jun 20 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.6-1
- Update to 0.5.6

* Thu Jun 15 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.5-1
- Update to 0.5.5

* Tue Jun 13 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.3-1
- Update to 0.5.3

* Fri Jun 09 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.2-1
- Update to 0.5.2

* Fri Jun 09 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.0-1
- Update to 0.5.0

* Sun Jun 04 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.3-2
- Removed dependency on pamixer

* Sun Jun 04 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.3-1
- Update to 0.4.3

* Mon Feb 27 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.2-1
- Update to 0.4.2

* Sun Jan 01 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.9-3
- Removed Arch Linux specific scripts
- Presets patched to remove Arch Linux specific calls

* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.9-2
- Fixed dependencies

* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.9-1
- Initial build
