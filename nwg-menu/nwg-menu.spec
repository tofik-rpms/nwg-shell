# Generated by go2rpm 1.9.0
%bcond_without check

# https://github.com/nwg-piotr/nwg-menu
%global goipath         github.com/nwg-piotr/nwg-menu
Version:                0.1.7

%gometa -f


%global common_description %{expand:
The nwg-menu command displays the system menu with simplified freedesktop
main categories (8 instead of 13). It also provides the search entry,
to look for installed application on the basis of .desktop files,
and for files in XDG user directories.

You may pin applications by right-clicking them. Pinned items will appear
above the categories list. Right-click a pinned item to unpin it.
The pinned items cache is shared with the nwggrid command,
which is a part of nwg-launchers.}

%global golicenses      LICENSE
%global godocs          README.md

Name:           nwg-menu
Release:        1%{?dist}
Summary:        MenuStart plugin to nwg-panel, also capable of working standalone

License:        MIT
URL:            %{gourl}
Source:         %{gosource}

BuildRequires:  golang(github.com/allan-simon/go-singleinstance)
BuildRequires:  golang(github.com/dlasky/gotk3-layershell/layershell)
BuildRequires:  golang(github.com/gotk3/gotk3/gdk)
BuildRequires:  golang(github.com/gotk3/gotk3/glib)
BuildRequires:  golang(github.com/gotk3/gotk3/gtk)
BuildRequires:  golang(github.com/joshuarubin/go-sway)

%description %{common_description}

%gopkg

%prep
%goprep
%autopatch -p1

%generate_buildrequires
%go_generate_buildrequires

%build
%gobuild -o %{gobuilddir}/bin/nwg-menu %{goipath}

%install
%gopkginstall
install -m 0755 -vd                       %{buildroot}%{_bindir}
install -m 0755 -vd                       %{buildroot}%{_datadir}/%{name}
install -m 0755 -vd                       %{buildroot}%{_datadir}/%{name}/desktop-directories
install -m 0755 -vp %{gobuilddir}/bin/*   %{buildroot}%{_bindir}/
install -m 0644 -vp menu-start.css        %{buildroot}/%{_datadir}/%{name}/
install -m 0644 -vp desktop-directories/* %{buildroot}/%{_datadir}/%{name}/desktop-directories/

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{_datadir}/%{name}

%gopkgfiles

%changelog
* Sat Jan 11 2025 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.7-1
- Update to 0.1.7

* Sun Sep 22 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.6-1
- Update to 0.1.6

* Sat Sep 21 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.5-1
- Update to 0.1.5

* Fri Aug 30 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.4-1
- Update to 0.1.4

* Mon Feb 19 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.3-1
- Update to 0.1.3

* Sun Jan 28 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.2-1
- Update to 0.1.2

* Tue Aug 01 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.1-2
- Golang packaging guidelines spec file

* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.1-1
- Initial build
