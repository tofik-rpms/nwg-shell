# -*-Mode: rpm-spec -*-

%global   forgeurl https://github.com/nwg-piotr/nwg-shell-config
%global sys_name nwg_shell_config

Name:           nwg-shell-config
Version:        0.5.58
%forgemeta
Summary:        Allows to set the common sway preferences
Release:        1%{?dist}

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       python-gobject
Requires:       python-i3ipc
Requires:       python-geopy

Recommends:     gtklock
Recommends:     playerctl
Recommends:     gammastep

%description
Nwg-shell-config allows to set the common sway preferences
(screen, input devices, idle and lock screen configuration,
main applications key bindings), as well as switch between
predefined desktop styles. Each of the above may be adjusted
to user’s taste, that includes placement of the application drawer,
dock, exit menu, and notification center.

%prep
%forgesetup -a

%build
%py3_build

%install
%py3_install
for lib in %{buildroot}%{python3_sitelib}/%{sys_name}/*.py; do
sed '1{@^#!/usr/bin/env python@d}' $lib > $lib.new &&
touch -r $lib $lib.new &&
mv $lib.new $lib
done
install -m644 -D -t %{buildroot}%{_datadir}/pixmaps nwg-shell-config.svg
install -m644 -D -t %{buildroot}%{_datadir}/pixmaps nwg-shell-update.svg
install -m644 -D -t %{buildroot}%{_datadir}/applications nwg-shell-config.desktop

%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{python3_sitelib}/%{sys_name}/
%{python3_sitelib}/%{sys_name}-%{version}-py%{python3_version}.egg-info/
%{_datadir}/pixmaps/*
%{_datadir}/applications/*

%changelog
* Mon Feb 17 2025 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.58-1
- Update to 0.5.58

* Fri Feb 14 2025 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.57-1
- Update to 0.5.57

* Wed Jan 29 2025 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.56-1
- Update to 0.5.56

* Fri Dec 20 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.55-1
- Update to 0.5.55

* Mon Dec 09 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.54-1
- Update to 0.5.54

* Sat Dec 07 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.52-1
- Update to 0.5.52

* Sun Nov 10 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.51-1
- Update to 0.5.51

* Fri Nov 01 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.50-1
- Update to 0.5.50

* Fri Oct 25 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.49-1
- Update to 0.5.49

* Thu Oct 24 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.48-1
- Update to 0.5.48

* Thu Oct 17 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.47-1
- Update to 0.5.47

* Wed Aug 21 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.46-1
- Update to 0.5.46

* Tue Aug 20 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.43-1
- Update to 0.5.43

* Sun Jun 30 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.42-1
- Update to 0.5.42

* Sun Jun 23 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.41-1
- Update to 0.5.41

* Wed Jun 12 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.40-1
- Update to 0.5.40

* Tue Jun 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.39-1
- Update to 0.5.39

* Mon May 20 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.38-1
- Update to 0.5.38

* Sat May 04 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.37-1
- Update to 0.5.37

* Fri Apr 19 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.36-1
- Update to 0.5.36

* Thu Apr 18 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.35-1
- Update to 0.5.35

* Wed Feb 07 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.34-1
- Update to 0.5.34

* Mon Feb 05 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.33-1
- Update to 0.5.33

* Sat Jan 27 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.32-1
- Update to 0.5.32

* Tue Jan 09 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.31-1
- Update to 0.5.31

* Thu Jan 04 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.30-1
- Update to 0.5.30

* Tue Dec 05 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.29-1
- Update to 0.5.29

* Fri Dec 01 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.28-1
- Update to 0.5.28

* Wed Nov 22 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.26-1
- Update to 0.5.26

* Fri Nov 17 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.25-1
- Update to 0.5.25

* Mon Nov 13 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.24-1
- Update to 0.5.24

* Thu Nov 09 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.23-1
- Update to 0.5.23

* Wed Oct 11 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.22-1
- Update to 0.5.22

* Tue Oct 10 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.21-1
- Update to 0.5.21

* Wed Oct 04 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.20-1
- Update to 0.5.20

* Tue Oct 03 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.19-1
- Update to 0.5.19

* Fri Sep 15 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.18-1
- Update to 0.5.18

* Fri Sep 01 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.17-1
- Update to 0.5.17

* Sat Aug 05 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.14-1
- Update to 0.5.14

* Fri Jul 28 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.13-1
- Update to 0.5.13

* Sat Jul 22 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.12-1
- Update to 0.5.12

* Sun Jul 16 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.11-1
- Update to 0.5.11

* Sun Jul 02 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.9-1
- Update to 0.5.9

* Sat Jul 01 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.8-1
- Update to 0.5.8

* Sun Jun 18 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.7-1
- Update to 0.5.7

* Tue Jun 13 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.6-1
- Update to 0.5.6

* Fri Jun 09 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.5-1
- Update to 0.5.5

* Mon Jun 05 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.2-1
- Update to 0.5.2

* Sun Jun 04 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.0-2
- Added gammastep as recommended package

* Sun Jun 04 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.5.0-1
- Update to 0.5.0

* Mon Feb 27 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.14-1
- Update to 0.4.14

* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.10-2
- Added dependencies

* Thu Dec 29 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.4.10-1
- Initial build
