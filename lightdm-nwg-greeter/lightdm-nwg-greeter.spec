%global forgeurl https://github.com/nwg-piotr/lightdm-nwg-greeter
Name:           lightdm-nwg-greeter
Version:        0.1.1
%forgemeta
Release:        2%{?dist}
Summary:        LightDM nwg greater

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}
Source1:        90-lightdm-nwg-greeter.conf

BuildArch:      noarch

Requires:       cage
Requires:       python(abi) >= 3.8
Requires:       python-gobject
Requires:       lightdm

%description
This greeter is based on the LightDM Elephant Greeter by Maximilian Moser
and has been preconfigured for use with nwg-iso.
This is a part of the nwg-shell project.

%prep
%forgeautosetup


%build
sed -e "s|INSTALL_PATH|%{_prefix}|" nwg-greeter.conf.base > nwg-greeter.conf

%install
install -D -m 644 -t %{buildroot}%{_sysconfdir}/lightdm/ nwg-greeter.conf
install -D -m 755 -t %{buildroot}%{_bindir} nwg-greeter.py
install -D -m 644 -t %{buildroot}%{_datadir}/lightdm/greeters lightdm-nwg-greeter.desktop
install -D -m 644 -t %{buildroot}%{_datadir}/lightdm/lightdm.conf.d %{SOURCE1}
install -D -m 644 -t %{buildroot}%{_datadir}/xgreeters lightdm-nwg-greeter-x11.desktop
install -D -m 644 -t %{buildroot}%{_datadir}/nwg-greeter nwg-greeter.ui
install -D -m 644 -t %{buildroot}%{_datadir}/nwg-greeter/img img/*
install -D -m 644 -t %{buildroot}%{_datadir}/nwg-greeter/lang lang/*


%check


%files
%license LICENSE
%doc README.md
%config(noreplace) %{_sysconfdir}/lightdm/*
%{_bindir}/*
%{_datadir}/lightdm/greeters/*
%{_datadir}/lightdm/lightdm.conf.d/*
%{_datadir}/xgreeters/*
%{_datadir}/nwg-greeter

%changelog
* Mon Dec 25 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.1-2
- Added lightdm config enabling greeter

* Mon Dec 25 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.1-1
- Initial build

