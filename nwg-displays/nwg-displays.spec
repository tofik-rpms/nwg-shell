# -*-Mode: rpm-spec -*-

# Use 0 for release and 1 for git
Version:        0.3.24
%global   forgeurl https://github.com/nwg-piotr/nwg-displays
%forgemeta

%global sys_name nwg_displays

Name:           nwg-displays
Summary:        This program provides an intuitive GUI to manage multiple displays
Release:        1%{?dist}

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       python-gobject
Requires:       python-i3ipc
Requires:       gtk-layer-shell

%description
This program provides an intuitive GUI to manage multiple displays,
save outputs configuration and workspace-to-output assignments.

%prep
%forgeautosetup

%build
%py3_build

%install
%py3_install
for lib in %{buildroot}%{python3_sitelib}/%{sys_name}/*.py; do
sed '1{/^#!/usr/bin/env python/d}' $lib > $lib.new &&
touch -r $lib $lib.new &&
mv $lib.new $lib
done
install -D -t %{buildroot}%{_datadir}/pixmaps nwg-displays.svg
install -D -t %{buildroot}%{_datadir}/applications nwg-displays.desktop

%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{python3_sitelib}/%{sys_name}/
%{python3_sitelib}/%{sys_name}-%{version}-py%{python3_version}.egg-info/
%{_datadir}/pixmaps/*
%{_datadir}/applications/*

%changelog
* Tue Mar 11 2025 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.24-1
- Update to 0.3.24

* Thu Feb 27 2025 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.23-1
- Update to 0.3.23

* Tue Oct 01 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.22-1
- Update to 0.3.22

* Wed Sep 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.21-1
- Update to 0.3.21

* Fri Aug 23 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.20-2
- Removed patch fixing version string

* Thu May 30 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.20-1
- Update to 0.3.20

* Sat May 18 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.19-1
- Update to 0.3.19

* Fri May 03 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.18-1
- Update to 0.3.18

* Wed May 01 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.17-1
- Update to 0.3.17

* Thu Mar 21 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.16-1
- Update to 0.3.16

* Wed Mar 20 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.15-1
- Update to 0.3.15

* Sat Mar 02 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.14-1
- Update to 0.3.14

* Sat Jan 27 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.13-1
- Update to 0.3.13

* Thu Jan 18 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.12-1
- Update to 0.3.12

* Tue Jan 16 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.11-1
- Update to 0.3.11

* Sun Dec 10 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.10-1
- Update to 0.3.10

* Sat Nov 25 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.9-1
- Update to 0.3.9

* Thu Sep 28 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.8-1
- Update to 0.3.8

* Thu Jul 13 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.7-1
- Update to 0.3.7

* Thu Jul 06 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.6-1
- Update to 0.3.6

* Sat Jul 01 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.5-1
- Update to 0.3.5

* Wed Jun 28 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.4-1
- Update to 0.3.4

* Sun Jun 04 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.3-1
- Update to 0.3.3
* Fri Dec 30 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.4-1
- Initial build
