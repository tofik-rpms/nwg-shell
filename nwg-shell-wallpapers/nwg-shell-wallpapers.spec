%global debug_package %{nil}
%global forgeurl  https://github.com/nwg-piotr/nwg-shell-wallpapers

Name:           nwg-shell-wallpapers
Version:        1.5
%forgemeta
Release:        1%{?dist}
Summary:        nwg-shell wallpapers package
BuildArch:      noarch
License:        CC0-1.0
URL:            %{forgeurl}
Source0:        %{forgesource}

%description
%{summary}

%prep
%forgeautosetup


%build


%install
install -d %{buildroot}%{_datadir}/backgrounds/nwg-shell
install -Dm644 wallpapers/* %{buildroot}%{_datadir}/backgrounds/nwg-shell

%check


%files
%license LICENSE
%dir %{_datadir}/backgrounds/nwg-shell
%{_datadir}/backgrounds/nwg-shell/*


%changelog
* Sat Feb 17 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.5-1
- Update to 1.5

* Sun Jan 28 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.4-1
- Update to 1.4

* Mon Oct 09 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.3-1
- Update to 1.3

* Tue Sep 19 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2-1
- Update to 1.2

* Sat Dec 31 2022 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.1-1
- Initial build
